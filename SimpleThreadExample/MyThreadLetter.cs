﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadExample
{
    class MyThreadLetter
    {
        private char Letter;
        public static bool Stop;
        Random rnd = new Random();
        private Thread LaTache;

        public char letter { get => Letter; set => Letter = value; }
        public Thread laTache { get => LaTache; set => LaTache = value; }
        public bool stop { get => Stop; set => Stop = value; }

        public AutoResetEvent SignalExit { get; } = new AutoResetEvent(false);

        public AutoResetEvent SignalVoyelle { get; } = new AutoResetEvent(false);

        private void RunLetter()
        {
            Console.WriteLine("\t\t---> Le threadLetter commence son travail.");

            while (!SignalExit.WaitOne(rnd.Next(100, 1001), false))
            {
                Letter = (char)rnd.Next(97, 123);
                 switch (Letter)
                 {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u': 
                    case 'y':
                    SignalVoyelle.Set();
                    break;
                    default:
                    break;
                 }
        }

        Console.WriteLine("\t\t---> Le threadLetter a fini son travail.");
        }
        public MyThreadLetter()
        {

            // Création d'un thread, sans le démarrer
           LaTache = new Thread(new ThreadStart(RunLetter));
        }
            public void Start()
            {

 
              try
              {
                LaTache.Start();
                Console.WriteLine("Letter démarré correctement...");
              }

 
              catch (ThreadStateException ex)
              {
                Console.WriteLine("Letter a déjà été démarré...");
              }

            }

    }
}
