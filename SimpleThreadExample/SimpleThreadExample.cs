﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadExample
{
    class SimpleThreadExample
    {
        static void Main(string[] args)
        {
            int iter = 15;
            // Création des objets encapsulant les threads.
            MyThreadLetter ThLetter = new MyThreadLetter();
            Console.WriteLine("> Le thread est créé.");
            ThLetter.Start();
            Console.WriteLine("> En route pour {0}sec. ...", iter);

            for (int i = 0; i < iter; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("> main: " + i + "sec.");
            }

            Console.WriteLine("> Demmande d’arrêt du thread.");
            // Beurk! Abort c'est mal !!
            //ThLetter.LaTache.Abort();
            ThLetter.SignalExit.Set();

            // On attend que le thread en cours soit terminés avant de continuer.
            ThLetter.laTache.Join();
            Console.WriteLine("> Les threads sont terminés.");
            Console.WriteLine("\n\n\t\tTouche \"Entrée\" pour terminer...");
            Console.ReadLine();

            //////////////////////////////////////////////////////////////////////////////////////////////////
            MyThreadNumber ThCptr = new MyThreadNumber();
            ThCptr.Start(19);
            ThCptr.stop = true;
            ThCptr.laTache.Join();
        }
    }
}
